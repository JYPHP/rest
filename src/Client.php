<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
declare(strict_types = 1);
namespace JYPHP\Rest;

use JBZoo\HttpClient\HttpClient;
use JBZoo\HttpClient\Response;
use JYPHP\Rest\DataType\Data;
use JYPHP\Rest\DataType\Json;

class Client implements \ArrayAccess
{
    protected $params   = [];
    protected $dataType = Json::class;

    /**
     * @var HttpClient
     */
    protected $httpClient;

    public function dealResponse(Response $response)
    {
        if ($this->dataType && class_exists($this->dataType)) {
            $data_type = new $this->dataType();
            if ($data_type instanceof Data) {
                return $data_type->deal($response->getBody());
            }
        }
        return $response->getBody();
    }

    public function __construct(?HttpClient $http_client = null)
    {
        $http_client      = $http_client ?: new HttpClient();
        $this->httpClient = $http_client;
    }

    public function get(string $url, ?array $params = null)
    {
        $params = $params ?: $this->params;
        return $this->dealResponse($this->httpClient->request($url, $params, "GET"));
    }

    public function delete(string $url, ?array $params = null)
    {
        $params = $params ?: $this->params;
        return $this->dealResponse($this->httpClient->request($url, $params, "DELETE"));
    }

    public function patch(string $url, ?array $params = null)
    {
        $params = $params ?: $this->params;
        return $this->dealResponse($this->httpClient->request($url, $params, "PATCH"));
    }

    public function options(string $url, ?array $params = null)
    {
        $params = $params ?: $this->params;
        return $this->dealResponse($this->httpClient->request($url, $params, "OPTIONS"));
    }

    public function post(string $url, ?array $params = null)
    {
        $params = $params ?: $this->params;
        return $this->dealResponse($this->httpClient->request($url, $params, "POST"));
    }

    public function put(string $url, ?array $params = null)
    {
        $params = $params ?: $this->params;
        return $this->dealResponse($this->httpClient->request($url, $params, "PUT"));
    }

    public function head(string $url, ?array $params = null)
    {
        $params = $params ?: $this->params;
        return $this->dealResponse($this->httpClient->request($url, $params, "HEAD"));
    }

    public function offsetExists($offset)
    {
        return isset($this->params[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->params[$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->params[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->params[$offset]);
    }
}