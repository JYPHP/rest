<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
namespace JYPHP\Rest\DataType;

use JYPHP\Rest\Exception\DataTypeException;

class Json extends Data
{

    /**
     * 处理数据
     * @param string $body
     * @return mixed
     * @throws DataTypeException
     */
    public function deal(string $body)
    {
        try {
            return json_decode($body);
        } catch (\Exception $exception) {
            throw new DataTypeException("$body : " . $exception->getMessage());
        }
    }
}