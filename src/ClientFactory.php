<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
declare(strict_types = 1);
namespace JYPHP\Rest;

use JBZoo\HttpClient\HttpClient;

/**
 * @see Client
 * @method static get($url, $params = null) 发送Get请求
 * @method static delete($url, $params = null) 发送delete请求
 * @method static patch($url, $params = null) 发送patch请求
 * @method static options($url, $params = null) 发送options请求
 * @method static post($url, $params = null) 发送post请求
 * @method static put($url, $params = null) 发送put请求
 * @method static head($url, $params = null) 发送head请求
 * @package JYPHP\Rest
 */
final class ClientFactory
{
    protected static $options = [
        'timeout' => 3,
        'verify' => false,
        'headers' => [

        ],
        'exceptions' => false,
        'allow_redirects' => false,
        'max_redirects' => 10,
        'user_agent' => 'JYPHP'
    ];

    /**
     * 设置参数
     * @param  $options
     * @param  null $value
     * @return ClientFactory
     */
    public static function setOptions($options, $value = null)
    {
        if ($value === null && is_array($options)) {
            self::$options = $options;
        } else {
            self::$options[$options] = $value;
        }
        return new self();
    }

    public static function __callStatic($name, $arguments)
    {
        $client = new Client(new HttpClient(self::$options));
        if (!method_exists($client, $name)) {
            throw new \Exception("$name is undefined", 500);
        }
        return $client->$name(...$arguments);
    }

    public function __call($name, $arguments)
    {
        $client = new Client(new HttpClient(self::$options));
        if (!method_exists($client, $name)) {
            throw new \Exception("$name is undefined", 500);
        }
        return $client->$name(...$arguments);
    }
}